defmodule Cards do
  @moduledoc """
    Provee metodos para la creacion y manejo de mazos de cartas
  """

  @doc """
    Devuelve una lista de strings representando las cartas en el mazo
  """
  def create_deck do
    values = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"]
    suits = [ "Spades", "Clubs", "Hearts", "Diamonds" ]

    for suit <- suits, value <- values do
        "#{value} of #{suit}"
    end
  end

  def shuffle( deck ) do
    Enum.shuffle( deck )
  end

  @doc """
    Responde si una carta está en el mazo o no.

  ## Ejemplos

      iex> deck = Cards.create_deck
      iex> Cards.contains?( deck, "Four of Diamonds" )
      true

  """
  def contains?( deck, card ) do
    Enum.member?( deck, card )
  end

  @doc """
    Divide un mazo en una `mano` del tamaño especificado, y 
    retorna el resto como el nuevo mazo.

  ## Ejemplos
      iex> deck = Cards.create_deck
      iex> {hand, deck} = Cards.deal( deck, 1 )
      iex> hand
      ["Ace of Spades"]

  """
  def deal( deck, hand_size ) do
    Enum.split( deck, hand_size )
  end

  def save( deck, filename ) do
    binary = :erlang.term_to_binary( deck )
    File.write( filename, binary )
  end

  def load( filename ) do
    case File.read( filename ) do
      {:ok, binary} -> {:ok, :erlang.binary_to_term( binary ) }
      {:error, _} -> {:error, "File does not exist"}
    end
  end

  def create_hand( hand_size ) do
    Cards.create_deck
    |> Cards.shuffle
    |> Cards.deal( hand_size )
  end
end
