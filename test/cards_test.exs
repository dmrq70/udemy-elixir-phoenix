defmodule CardsTest do
  use ExUnit.Case
  doctest Cards

  test "create_deck genera 52 cartas" do
    assert length( Cards.create_deck ) == 52
  end

  test "shuffle baraja las cartas del mazo" do
    deck = Cards.create_deck
    assert deck != Cards.shuffle( deck )
  end

end
